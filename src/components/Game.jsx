import { getAllActors, uniqueActors, getAllMoviesAndCast } from "../services.js";
import { Timer } from "./Timer.jsx";
import { useState, useEffect } from "react";
import '../utils.js'
import { useNavigate } from "react-router-dom";
import YesNo from "./YesNo.jsx";
import PosterAndActor from "./PosterAndActor.jsx";

const Game = () => {
    const [allMovies, setAllMovies] = useState([])
    const [score, setScore] = useState(0)
    const [randomActor, setRandomActor] = useState(null)
    const [currentMovieAndActors, setCurrentMovieAndActors] = useState()
    const navigate = useNavigate();  

    // prend un acteur random en s'assurant de ne pas faire de doublon dans la liste de nos films et dans la liste des acteurs populaires de l'api. 
    // au cas ou l'acteur est super connu et qu'il soit dans les deux listes.
    const getRandomActor = () => {
        getAllActors().then(response => {
            const allActors = uniqueActors(currentMovieAndActors.cast, response.results).filter(actor => actor.profile_path && actor.profile_path.length > 0)
            let responses = []
            if (allActors && allActors.length > currentMovieAndActors.cast.length) {
                responses = [...currentMovieAndActors.cast, ...allActors.slice(0, currentMovieAndActors.cast.length)];
            } else {
                responses = [...currentMovieAndActors.cast.slice(0, allActors.length), ...allActors];
            }
            setRandomActor(responses.random())
        })
    }

    useEffect(() => {
        // recupère les films populaires, les acteurs de ses films, le tout ajouté dans un state !
        getAllMoviesAndCast().then(setAllMovies)
    }, [])

    useEffect(() => {
        if (allMovies.length > 0) {
            setCurrentMovieAndActors(allMovies.random())
        }
    }, [allMovies, score])

    useEffect(() => {
        if (currentMovieAndActors) {
            getRandomActor()
        }
    }, [currentMovieAndActors])

    // si la réponse est correcte
    // si l'acteur random joue dans le film affiché +1 sinon redirection vers la page de score
    const isResponseCorrect = (response) => {
        if (currentMovieAndActors.cast.includes(randomActor) === response) {
            setScore(score + 1)
            console.log("score : ", score + 1)
            const highScore = window.localStorage.getItem('score');
            if (highScore < score) window.localStorage.setItem('score', score + 1);
        } else {
            navigate('/score', {
                state :  {
                    score
                }
            })
        }
    }

    // quand le temps est écoulé retour vers la page de score
    const timerElapsed = () => {
        const highScore = window.localStorage.getItem('score');
        if (highScore < score) window.localStorage.setItem('score', score);
        navigate('/score', {
            state :  {
                score
            }
        })
    }

    return (
        <div>
            <h1 className="text-center">Da Movie Quizz </h1>
            <div className="text-center display-4">Score : {score}</div>
            <PosterAndActor currentMovieAndActors={currentMovieAndActors} randomActor={randomActor} />
            <YesNo randomActor={randomActor} responseCallback={isResponseCorrect} />
            <Timer elapsed={timerElapsed} />
        </div>
    )
}

export default Game