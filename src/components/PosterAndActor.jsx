const PosterAndActor = ({currentMovieAndActors, randomActor}) => {
    return (
        <section className="d-flex justify-content-evenly align-items-center">
            <div className="d-flex flex-column align-items-center">
                <h3>
                    {currentMovieAndActors?.title || ''}
                </h3>
                <p>
                    {currentMovieAndActors && <img style={{maxWidth: 300, maxHeight: 500}}src={`https://image.tmdb.org/t/p/w500/${currentMovieAndActors?.poster_path}`} alt={currentMovieAndActors?.title || ''}/>}
                </p>
            </div>
           {randomActor&&randomActor.profile_path ? <img style={{maxWidth: 300, maxHeight: 450}} src={`https://image.tmdb.org/t/p/w500${randomActor.profile_path}`} alt={randomActor?.name || ''} /> : ""}
        </section>
    )
}


export default PosterAndActor