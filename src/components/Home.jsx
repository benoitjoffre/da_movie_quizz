import {Link} from 'react-router-dom'

const Home = () => {
    const highScore = window.localStorage.getItem('score');
    return (
        <div className="d-flex flex-column justify-content-center align-items-center">
            <h1>Bienvenue sur Da Movie Quizz</h1>
            <h2 className="mb-5">Clique sur GO, quand tu est prêt</h2>

            <Link className="btn btn-lg btn-primary" to="/game">GO</Link>

            <p className="mt-5" style={{fontWeight: 'bold'}}>Ton meilleur score : {highScore}</p>
        </div>
    )
}

export default Home