const YesNo = ({randomActor, responseCallback}) => {

    return (
        <section className="d-flex justify-content-center align-items-center flex-column">
            <h3>Est-ce que cet acteur joue dans le film ?</h3>
            <div className="d-flex flex-column">
                <p style={{fontWeight: "bold", fontSize: 30}}>{randomActor?.name || ''}</p>
                <span className="d-flex justify-content-evenly">
                    <div className="btn btn-lg btn-success" onClick={() => responseCallback(true)}>Oui</div>                    
                    <div className="btn btn-lg btn-danger" onClick={() => responseCallback(false)}>Non</div>                    
                </span>
            </div>
    </section>
    )
}

export default YesNo