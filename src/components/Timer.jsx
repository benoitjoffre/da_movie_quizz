import { useState, useEffect } from "react"

export const Timer = ({ elapsed }) => {
    const [time, setTime] = useState(60)

    useEffect(() => {
        let timer;
        if (time > 0) {
            timer = setInterval(() => setTime(time - 1), 1000)
        } else {
            elapsed()
        }
        return () => clearInterval(timer);
    }, [elapsed, time])

    return <p style={{fontWeight: "bold", fontSize: 30}} className="text-center text-uppercase">Timer : {time} seconds</p>
}