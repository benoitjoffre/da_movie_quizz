import { Link } from "react-router-dom"
import {useLocation} from 'react-router-dom';

const Score = () => {
    const location = useLocation()
    const score = location.state.score
    const highScore = window.localStorage.getItem('score');
    return (
        <div className="d-flex flex-column justify-content-center align-items-center">
            <h1>Ton score est de {score} points !</h1>
            <h2>Ton meilleur score est de {highScore} points !</h2>
            <Link className="btn btn-lg btn-primary mt-5" to='/game'>Relancer le jeu !</Link>
        </div>
    )
}

export default Score