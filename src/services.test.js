import { uniqueActors } from './services'

test('deduplicate lists', () => {
    const listA = [{id: 1}, {id: 2}, {id: 3}]
    const listB = [{id: 1}, {id: 4}, {id: 6}]
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual([{id: 4}, {id: 6}])
});
test('deduplicate lists must be empty if all equals', () => {
    const listA = [{id: 1}, {id: 2}, {id: 3}]
    const listB = [{id: 1}, {id: 2}]
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual([])
});
test('deduplicate lists if no match', () => {
    const listA = [{id: 1}, {id: 2}, {id: 3}]
    const listB = [{id: 5}, {id: 8}]
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual(listB)
});
test('deduplicate lists if base list is empty', () => {
    const listA = []
    const listB = [{id: 5}, {id: 8}]
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual(listB)
});
test('deduplicate lists if base list is undefined', () => {
    const listA = undefined
    const listB = [{id: 5}, {id: 8}]
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual(listB)
});
  
test('if deduplicate list is undefined', () => {
    const listA = [{id: 5}, {id: 8}]
    const listB = undefined
    const deduplicatedList = uniqueActors(listA, listB)
    expect(deduplicatedList).toStrictEqual([])
});
  