import Game from "./components/Game.jsx";
import Home from "./components/Home.jsx";
import Score from "./components/Score.jsx";
import { Routes, Route } from "react-router-dom";
const App = () => {
  const highScore = window.localStorage.getItem('score');
  if (highScore === undefined) window.localStorage.setItem('score', 0);
  
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="game" element={<Game />} />
        <Route path="score" element={<Score/>} />
      </Routes>
    </div>
      
  );
}

export default App;
