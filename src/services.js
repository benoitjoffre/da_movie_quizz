const API_KEY = "7ea5f490261a949e52930517e1b4657c"

// fonction qui evite de créer des doublons. si l'acteur est populaire et qu'il est dans le casting du film et dans les acteurs populaires il va y être deux fois..
export const uniqueActors = (castMembers, randomActors) => {
    if (randomActors === undefined) return []
    return randomActors.filter(actor => {
        if (castMembers === undefined) return true;
        const castMember = castMembers.find(caster => actor.id === caster.id)
        return castMember === undefined
    });
}


export const getAllMoviesAndCast = () => {
    return getMovies().then(movies => {
        return movies.results.map((movie) => {
            getActors(movie.id).then(response => {
                movie.cast = response.cast.filter(actor => actor.profile_path && actor.profile_path.length > 0); // filtre les acteurs sans photo car c'est plus cool de voir une photo !
            })
            return movie;
        })
    })
}

// GET ALL POPULAR MOVIES
export const getMovies = async () => {
    const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=fr`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    return await handleResponse(response)
}

// GET A MOVIE CREDITS
export const getActors = async (film_id) => {
    const response = await fetch(`https://api.themoviedb.org/3/movie/${film_id}/credits?api_key=${API_KEY}&language=fr`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    return await handleResponse(response)
}

// GET POPULAR ACTORS
export const getAllActors = async () => {
    const response = await fetch(`https://api.themoviedb.org/3/person/popular?api_key=${API_KEY}&language=fr`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json'},
    })
    return await handleResponse(response)
}

const handleResponse = async response => {
    if (response.status >= 400 && response.status <= 499) {
        throw new Error("Nom de zeus une erreur est survenue Marty !")
    }
    if (response.status >= 500 && response.status <= 599) {
        throw new Error("Erreur 5XX")
    }
    if (response.status >= 300 && response.status <= 399) {
        throw new Error("Erreur 3XX")
    }
    return await response.json()
}